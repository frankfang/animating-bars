type Child = string | number | Element
const createElement = (name: string, props?: any, children?: Array<Child> | Child): HTMLElement => {
  const elements = children
    ? (children instanceof Array ? children : [children])
    : []
  const tag = document.createElement(name)
  const { style, dataset, class: className, ...rest } = props ?? {}
  tag.className = className
  Object.assign(tag, rest)
  Object.assign(tag.style, props?.style)
  Object.assign(tag.dataset, props?.dataset)
  elements.map((child) => tag.append(child instanceof Element ? child : child.toString()))
  return tag
}

export { createElement }